package by.kolovaitis.spp.presentation.dialogs

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import by.kolovaitis.spp.domain.model.User
import by.kolovaitis.spp.domain.usecase.AllUsecase
import by.kolovaitis.spp.domain.usecase.LogoutUsecase
import by.kolovaitis.spp.domain.usecase.PairsUsecase
import by.kolovaitis.spp.domain.usecase.ProfileUsecase
import by.kolovaitis.spp.presentation.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PairViewModel(
    private val pairsUsecase: PairsUsecase,private val profileUsecase: ProfileUsecase, logoutUsecase: LogoutUsecase, context: Context,
) : BaseViewModel(logoutUsecase, context) {
    private val _pairs: MutableLiveData<List<User>> by lazy {
        MutableLiveData()
    }
    val pairs: LiveData<List<User>> get() = _pairs

    init {
        viewModelScope.launch(Dispatchers.IO) {
            runSafe {
                _pairs.postValue(pairsUsecase().map { profileUsecase(it) })
            }
        }
    }


}