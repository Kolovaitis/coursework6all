package by.kolovaitis.spp.domain.model

import android.net.Uri
import android.os.Parcelable
import java.util.*

data class User(
    val name: String?=null,
    val image: Uri?=null,
    val description: String?=null,
    val id: Int?=null,
    val dateOfBirth: Date?=null,
    val longitude: Double?=null,
    val latitude: Double?=null,
)
