package by.kolovaitis.spp.data.repository.impl

import by.kolovaitis.spp.data.datasource.FirebaseDatasource
import by.kolovaitis.spp.data.datasource.RetrofitDatasource
import by.kolovaitis.spp.data.model.LoginData
import by.kolovaitis.spp.data.model.RegisterData
import by.kolovaitis.spp.domain.model.User
import by.kolovaitis.spp.domain.repository.LoginRepository

class RetrofitLoginRepository(
    private val retrofitDatasource: RetrofitDatasource,
    private val firebaseDatasource: FirebaseDatasource
) :
    LoginRepository {
    override suspend fun login(email: String, password: String) {
        firebaseDatasource.login(email, password)
        retrofitDatasource.login(LoginData(email = email, password = password))
    }

    override suspend fun logout() {
        retrofitDatasource.logout()
    }

    override suspend fun register(email: String, password: String, user: User) {
        firebaseDatasource.register(email, password)
        retrofitDatasource.register(
            RegisterData(
                email = email,
                password = password,
                name = user.name!!,
                date_of_birth = user.dateOfBirth.toString(),
                description = user.description!!,
                image = firebaseDatasource.loadImage(user.image!!),
                latitude = user.latitude!!,
                longitude = user.longitude!!,
            )
        )
    }

    override fun isUserLogin() =
        retrofitDatasource.isUserLogin()


}