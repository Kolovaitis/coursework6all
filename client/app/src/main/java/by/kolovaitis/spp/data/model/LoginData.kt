package by.kolovaitis.spp.data.model

data class LoginData(val email: String, val password: String)
data class RegisterData(
    val email: String,
    val password: String,
    val name: String,
    val date_of_birth: String,
    val longitude: Double,
    val latitude: Double,
    val image: String,
    val description: String,
)

data class LoginResponseData(val token: String)