package by.kolovaitis.spp.presentation.dialogs

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class UserData(val id: Int, val name: String?):Parcelable
