package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.repository.DataRepository

class WatchUsecase(private val dataRepository: DataRepository) {
    suspend operator fun invoke(id:Int) {
        return dataRepository.watchUser(id)
    }
}