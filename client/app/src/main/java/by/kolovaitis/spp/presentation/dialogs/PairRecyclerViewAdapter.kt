package by.kolovaitis.spp.presentation.dialogs

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import by.kolovaitis.spp.R
import by.kolovaitis.spp.databinding.FragmentPairBinding
import by.kolovaitis.spp.domain.model.User
import com.squareup.picasso.Picasso

import java.util.*

/**
 * [RecyclerView.Adapter] that can display a [PlaceholderItem].
 * TODO: Replace the implementation with code for your data type.
 */
class PairRecyclerViewAdapter(
    private var values: List<User>,
    val click:(User)->Unit
) : RecyclerView.Adapter<PairRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            FragmentPairBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        Picasso.get().load(item.image).placeholder(R.drawable.ic_user).into(holder.imageView)
        holder.textView.text = item.name
        holder.root.setOnClickListener { click(item) }
    }

    override fun getItemCount(): Int = values.size
    fun update(new:List<User>){
        values = new
        notifyDataSetChanged()
    }

    inner class ViewHolder(binding: FragmentPairBinding) : RecyclerView.ViewHolder(binding.root) {
        val imageView:ImageView = binding.imageView
        val textView: TextView = binding.textView
        val root: View = binding.root

        override fun toString(): String {
            return super.toString() + " '" + textView.text + "'"
        }
    }

}