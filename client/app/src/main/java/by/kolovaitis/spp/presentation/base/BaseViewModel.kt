package by.kolovaitis.spp.presentation.base

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import by.kolovaitis.spp.domain.usecase.LogoutUsecase
import by.kolovaitis.spp.presentation.login.LoginActivity
import retrofit2.HttpException
import java.lang.Exception

open class BaseViewModel(val logoutUsecase: LogoutUsecase, val context: Context) : ViewModel() {
    private val _wasError: MutableLiveData<String> by lazy {
        MutableLiveData()
    }

    val wasError: LiveData<String> get() = _wasError

    protected val _isLoading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val isLoading: LiveData<Boolean> get() = _isLoading

    fun showError(message: String?) {
        _wasError.postValue(message)
    }

    protected suspend inline fun runSafe(code: () -> Unit) {
        _isLoading.postValue(true)
        try {
            code()
        } catch (e: Exception) {
            Log.e("EXCEPTION", e.localizedMessage)
            if (e is HttpException && (e as HttpException).code() == 401){
                logoutUsecase()
            }
                showError(e.localizedMessage)
        }
        _isLoading.postValue(false)
    }
}