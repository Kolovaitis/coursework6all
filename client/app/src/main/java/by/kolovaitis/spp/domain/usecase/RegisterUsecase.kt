package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.model.User
import by.kolovaitis.spp.domain.repository.LoginRepository

class RegisterUsecase(private val loginRepository: LoginRepository) {
    suspend operator fun invoke(email:String, password:String, user: User){
        loginRepository.register(email, password, user)
    }
}