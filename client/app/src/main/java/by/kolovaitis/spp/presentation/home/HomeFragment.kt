package by.kolovaitis.spp.presentation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import by.kolovaitis.spp.R
import by.kolovaitis.spp.databinding.FragmentHomeBinding
import com.squareup.picasso.Picasso
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class HomeFragment : Fragment() {

    private val vm: HomeViewModel by viewModel()
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.bLike.setOnClickListener {
            vm.like()
        }
        binding.bWatch.setOnClickListener {
            vm.dislike()
        }
        with(vm) {
            currentProfile.observe(viewLifecycleOwner) {
                Picasso.get().load(it.image).placeholder(R.drawable.ic_user).into(binding.ivImage)
                val current = Date()
                binding.tvName.text = it.name+ ", " + with(it.dateOfBirth) {
                    if (current.month >= this!!.month) {
                        current.year - year
                    } else {
                        current.year - year - 1
                    }.toString()
                }
                binding.tvDescription.text = it.description
            }
            isLoading.observe(viewLifecycleOwner) {
                binding.cvUser.visibility = if (it) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
                binding.progressBar.visibility = if (!it) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
    }
}