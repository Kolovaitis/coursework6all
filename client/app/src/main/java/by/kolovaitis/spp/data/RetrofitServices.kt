package by.kolovaitis.spp.data

import by.kolovaitis.spp.data.model.*
import retrofit2.http.*

interface RetrofitServices {
    @Headers("Content-Type: application/json")
    @POST("/mark/{id}")
    suspend fun mark(
        @Path(value = "id") id: Int,
        @Body mark: MarkData,
        @Header("Authorization") token: String
    )

    @Headers("Content-Type: application/json")
    @POST("/messages/{id}")
    suspend fun sendMessage(
        @Path(value = "id") id: Int,
        @Body message: MessageData,
        @Header("Authorization") token: String
    )
    @GET("/messages/{id}")
    suspend fun getMessagesWith(
        @Path(value="id")id:Int,
        @Header(
            "Authorization"
        ) token: String
    ): MessagesResponse

    @GET("/next")
    suspend fun next(
        @Header(
            "Authorization"
        ) token: String
    ): UserData
    @GET("/all")
    suspend fun all(
        @Header(
            "Authorization"
        ) token: String
    ): ProfilesResponse

    @GET("/pairs")
    suspend fun pairs(
        @Header(
            "Authorization"
        ) token: String
    ): PairsResponse
    @GET("/profile/{id}")
    suspend fun profile(
        @Path(value="id")id:Int,
        @Header(
            "Authorization"
        ) token: String
    ): ProfileResponse
    @GET("/profile")
    suspend fun profile(
        @Header(
            "Authorization"
        ) token: String
    ): ProfileResponse

    @Headers("Content-Type: application/json")
    @POST("/registration")
    suspend fun registration(
        @Body body: RegisterData
    ): LoginResponseData

    @Headers("Content-Type: application/json")
    @POST("/login")
    suspend fun login(
        @Body body: LoginData
    ): LoginResponseData
}