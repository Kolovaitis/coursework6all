package by.kolovaitis.spp.presentation.map


import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import by.kolovaitis.spp.domain.model.User
import by.kolovaitis.spp.domain.usecase.*
import by.kolovaitis.spp.presentation.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MapViewModel(
   private val allUsecase: AllUsecase, logoutUsecase: LogoutUsecase, context: Context,
) : BaseViewModel(logoutUsecase, context) {
    private val _users: MutableLiveData<List<User>> by lazy {
        MutableLiveData()
    }
    val users: LiveData<List<User>> get() = _users

    init {
        viewModelScope.launch(Dispatchers.IO) {
            runSafe {
               _users.postValue(allUsecase())
            }
        }
    }


}