package by.kolovaitis.spp.domain.repository

import by.kolovaitis.spp.data.model.*
import by.kolovaitis.spp.domain.model.Message
import by.kolovaitis.spp.domain.model.User

interface DataRepository {
    suspend fun likeUser(id: Int)
    suspend fun watchUser(id:Int)
    suspend fun sendMessage(id: Int, text:String)
    suspend fun allMessagesWith(id: Int): List<Message>
    suspend fun next(): Int?
    suspend fun all(): List<User>
    suspend fun pairs(): List<Int>
    suspend fun profile(idUser: Int): User
    suspend fun profile(): User
}