package by.kolovaitis.spp.presentation

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import by.kolovaitis.spp.R
import by.kolovaitis.spp.presentation.login.LoginActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.squareup.picasso.Picasso
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {
    private val vm: MainViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(vm.isUserLogin){
            setContentView(R.layout.activity_main)
            val navView: BottomNavigationView = findViewById(R.id.nav_view)
            val navController = findNavController(R.id.nav_host_fragment)
            val appBarConfiguration = AppBarConfiguration(
                setOf(
                    R.id.navigation_home,
                    R.id.navigation_map,
                    R.id.navigation_messages
                )
            )
            setupActionBarWithNavController(navController, appBarConfiguration)
            navView.setupWithNavController(navController)
        }else{
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId==R.id.action_settings){
            MaterialDialog(this).show {
                customView(R.layout.my_profile)
                this.getCustomView().apply {
                    findViewById<TextView>(R.id.tvName).text = vm.currentUser.value?.name
                    findViewById<TextView>(R.id.tvDescription).text = vm.currentUser.value?.description
                    Picasso.get().load(vm.currentUser.value?.image).placeholder(R.drawable.ic_user).into(findViewById<ImageView>(R.id.ivImage))
                }
            }
            return true
        }
        if(item.itemId==R.id.action_exit){
            vm.logout()
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}