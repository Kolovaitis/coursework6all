package by.kolovaitis.spp.presentation.dialogs

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import by.kolovaitis.spp.domain.model.Message
import by.kolovaitis.spp.domain.model.User
import by.kolovaitis.spp.domain.usecase.*
import by.kolovaitis.spp.presentation.base.BaseViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext

class MessageViewModel(
    val partner: UserData,
    private val messagesUsecase: AllMessagesUsecase,
    private val messageUsecase: SendMessageUsecase,
    private val profileUsecase: ProfileUsecase,
    logoutUsecase: LogoutUsecase,
    context: Context,
) : BaseViewModel(logoutUsecase, context) {
    private val _messages: MutableLiveData<List<Message>> by lazy {
        MutableLiveData()
    }
    val messages: LiveData<List<Message>> get() = _messages

    init {
        viewModelScope.launch(newSingleThreadContext("messages")) {
            while (true) {
                val newMessages = messagesUsecase(partner.id!!)
                if (newMessages.size != messages.value?.size)
                    _messages.postValue(newMessages)
                delay(300)
            }
        }
    }

    fun sendMessage(text: String) {
        viewModelScope.launch {
            runSafe {
                messageUsecase(partner.id!!, text)
            }
        }
    }

}