package by.kolovaitis.spp.domain.model

import java.util.*

data class Message(val from:Int?, val to:Int?, val text:String, val date:Date?)
