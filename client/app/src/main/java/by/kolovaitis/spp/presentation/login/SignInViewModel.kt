package by.kolovaitis.spp.presentation.login

import android.content.Context
import androidx.lifecycle.*
import by.kolovaitis.spp.domain.usecase.LoginUsecase
import by.kolovaitis.spp.domain.usecase.LogoutUsecase
import by.kolovaitis.spp.presentation.base.BaseViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext

class SignInViewModel(
    private val loginUsecase: LoginUsecase, logoutUsecase: LogoutUsecase,
    context: Context
) : BaseViewModel(
    logoutUsecase, context
) {
    private val _email: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val email: LiveData<String> get() = _email

    private val _password: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val password: LiveData<String> get() = _password

    private val _isButtonActive: MediatorLiveData<Boolean> by lazy {
        MediatorLiveData<Boolean>().apply {
            val obserever = Observer<Any> {
                if (_email.value.isNullOrEmpty() || _password.value.isNullOrEmpty() || isLoading.value == true) {
                    this.postValue(false)
                } else {
                    this.postValue(true)
                }
            }
            this.addSource(_email, obserever)
            this.addSource(_password, obserever)
            this.addSource(isLoading, obserever)

        }
    }
    val isButtonActive: LiveData<Boolean> get() = _isButtonActive


    private val _successfullyLogin: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val successfullyLogin: LiveData<Boolean> get() = _successfullyLogin

    fun login() {
        viewModelScope.launch(context = newSingleThreadContext("")) {
            runSafe {
                if (loginUsecase(email.value!!, password.value!!) != null) {
                    _successfullyLogin.postValue(true)
                } else {
                    showError("Invalid params")
                }
            }
        }
    }

    fun emailHasChanged(newEmail: String) {
        _email.postValue(newEmail)
    }

    fun passwordHasChanged(newPassword: String) {
        _password.postValue(newPassword)
    }
}