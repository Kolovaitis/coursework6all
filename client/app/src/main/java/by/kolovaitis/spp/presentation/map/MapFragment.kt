package by.kolovaitis.spp.presentation.map

import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kolovaitis.spp.R
import by.kolovaitis.spp.databinding.FragmentMapBinding
import by.kolovaitis.spp.presentation.home.HomeViewModel
import com.squareup.picasso.Picasso
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.runtime.ui_view.ViewProvider
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MapFragment : Fragment() {

    private val vm: MapViewModel by viewModel()
    private lateinit var binding: FragmentMapBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMapBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapObjects = binding.mapview.map.mapObjects
        vm.users.observe(viewLifecycleOwner) {
            mapObjects.clear()
            for (user in it) {
                val userView =
                    LayoutInflater.from(view.context)
                        .inflate(R.layout.item_user_on_map, view as ViewGroup, false)

                val mapObject = mapObjects.addPlacemark(
                    Point(
                        user.latitude!!,
                        user.longitude!!,
                    ),
                    ViewProvider(userView)
                )


            }
        }
    }

    override fun onStart() {
        super.onStart()
        binding.mapview.onStart()
        MapKitFactory.getInstance().onStart()
    }

    override fun onStop() {
        super.onStop()
        binding.mapview.onStop()
        MapKitFactory.getInstance().onStop()
    }
}