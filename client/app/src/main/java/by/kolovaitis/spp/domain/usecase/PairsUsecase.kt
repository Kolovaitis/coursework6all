package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.repository.DataRepository

class PairsUsecase(private val dataRepository: DataRepository) {
    suspend operator fun invoke(): List<Int> {
       return dataRepository.pairs()
    }
}