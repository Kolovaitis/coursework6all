package by.kolovaitis.spp.presentation.dialogs

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import by.kolovaitis.spp.R
import by.kolovaitis.spp.databinding.FragmentMessageListBinding
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.input.getInputField
import com.afollestad.materialdialogs.input.input
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

/**
 * A fragment representing a list of Items.
 */
class MessageFragment : Fragment() {
    private val vm: MessageViewModel by viewModel { parametersOf(arguments?.get("partner")) }
    private var columnCount = 1
    private lateinit var adapter: MessageRecyclerViewAdapter
    private lateinit var binding: FragmentMessageListBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMessageListBinding.inflate(inflater)

        // Set the adapter
        adapter = MessageRecyclerViewAdapter(emptyList(), vm.partner)

        with(binding.list) {
            layoutManager = when {
                columnCount <= 1 -> LinearLayoutManager(context)
                else -> GridLayoutManager(context, columnCount)
            }
            adapter = this@MessageFragment.adapter
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.messages.observe(viewLifecycleOwner) {
            adapter.update(it)
        }
        binding.floatingActionButton.setOnClickListener {
            MaterialDialog(requireContext(), BottomSheet(LayoutMode.WRAP_CONTENT)).show {
                this.input()
                positiveButton {
                    vm.sendMessage(it.getInputField().text.toString())
                }

            }
        }
    }
}