package by.kolovaitis.spp.data.repository.impl

import by.kolovaitis.spp.data.datasource.FirebaseDatasource
import by.kolovaitis.spp.data.datasource.RetrofitDatasource
import by.kolovaitis.spp.data.model.MarkData
import by.kolovaitis.spp.data.model.MessageData
import by.kolovaitis.spp.domain.model.Message
import by.kolovaitis.spp.domain.model.User
import by.kolovaitis.spp.domain.repository.DataRepository
import java.util.*

class DataRepositoryImpl(
    private val retrofitDatasource: RetrofitDatasource,
    private val firebaseDatasource: FirebaseDatasource
) : DataRepository {
    override suspend fun likeUser(id: Int)=
        retrofitDatasource.markUser(id, MarkData(true))


    override suspend fun watchUser(id: Int) =
        retrofitDatasource.markUser(id, MarkData(false))

    override suspend fun sendMessage(id: Int, text: String) {
        retrofitDatasource.sendMessage(MessageData(to_id = id, text = text))
    }

    override suspend fun allMessagesWith(id: Int): List<Message> {
        return retrofitDatasource.allMessagesWith(id).map {
            Message(
                from = it.from_id,
                to = it.to_id,
                text = it.text!!,
                date = Date(it.date)
            )
        }
    }

    override suspend fun next(): Int? {
        return retrofitDatasource.next().id
    }

    override suspend fun all(): List<User> {
        return retrofitDatasource.all().map {
            User(
                longitude = it.longitude,
                latitude = it.latitude,
                id = it.id
            )
        }
    }

    override suspend fun pairs(): List<Int> {
        return retrofitDatasource.pairs()
    }

    override suspend fun profile(idUser: Int): User {
        return with(retrofitDatasource.profile(idUser)) {
            User(
                name = name,
                id = id,
                image = firebaseDatasource.getImageUrl(image!!),
                description = description,
                latitude = latitude,
                longitude = longitude,
                dateOfBirth = Date(date_of_birth),
            )
        }
    }

    override suspend fun profile(): User {
        return with(retrofitDatasource.profile()) {
            User(
                name = name,
                id = id,
                image = firebaseDatasource.getImageUrl(image!!),
                description = description,
                latitude = latitude,
                longitude = longitude,
                dateOfBirth = Date(date_of_birth),
            )
        }
    }
}