package by.kolovaitis.spp.modules

import android.content.Context
import by.kolovaitis.spp.data.datasource.FirebaseDatasource
import by.kolovaitis.spp.data.datasource.RetrofitDatasource
import by.kolovaitis.spp.data.datasource.impl.FirebaseDatasourceImpl
import by.kolovaitis.spp.data.datasource.impl.RetrofitPostDatasource
import by.kolovaitis.spp.data.repository.impl.DataRepositoryImpl
import by.kolovaitis.spp.data.repository.impl.RetrofitLoginRepository
import by.kolovaitis.spp.domain.model.User
import by.kolovaitis.spp.domain.repository.DataRepository
import by.kolovaitis.spp.domain.repository.LoginRepository
import by.kolovaitis.spp.domain.usecase.*
import by.kolovaitis.spp.presentation.MainViewModel
import by.kolovaitis.spp.presentation.dialogs.MessageViewModel
import by.kolovaitis.spp.presentation.dialogs.PairViewModel
import by.kolovaitis.spp.presentation.dialogs.UserData
import by.kolovaitis.spp.presentation.home.HomeViewModel
import by.kolovaitis.spp.presentation.login.SignInViewModel
import by.kolovaitis.spp.presentation.login.SignUpViewModel
import by.kolovaitis.spp.presentation.map.MapViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val componentsModule = module {
    single<RetrofitDatasource> { RetrofitPostDatasource(services = get(), get()) }
    single<FirebaseDatasource> { FirebaseDatasourceImpl(get(), get()) }
    single<DataRepository> { DataRepositoryImpl(get(), get()) }
    single<LoginRepository> { RetrofitLoginRepository(get(), get()) }
    factory { LoginUsecase(get()) }
    factory { RegisterUsecase(get()) }
    factory { LogoutUsecase(get()) }
    factory { AllMessagesUsecase(get()) }
    factory { AllUsecase(get()) }
    factory { LikeUsecase(get()) }
    factory { NextUsecase(get()) }
    factory { PairsUsecase(get()) }
    factory { ProfileUsecase(get()) }
    factory { SendMessageUsecase(get()) }
    factory { WatchUsecase(get()) }
    factory { IsUserLoginUsecase(get()) }

    viewModel { HomeViewModel(get(), get(), get(), get(), get(), get()) }
    viewModel { SignInViewModel(get(), get(), get()) }
    viewModel { SignUpViewModel(get(), androidContext(), get()) }
    viewModel { MainViewModel(get(), get(), get(), get()) }
    viewModel { MapViewModel(get(), get(), get()) }
    viewModel { PairViewModel(get(), get(), get(),get()) }
    viewModel {params-> MessageViewModel(partner= params.get<UserData>() as UserData, get(),get(),get(),get(),get()) }

}