package by.kolovaitis.spp.modules

import android.content.SharedPreferences
import android.preference.PreferenceManager
import by.kolovaitis.spp.BuildConfig
import by.kolovaitis.spp.data.RetrofitServices
import by.kolovaitis.spp.data.datasource.FirebaseDatasource
import by.kolovaitis.spp.data.datasource.impl.FirebaseDatasourceImpl
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    factory { provideOkHttpClient() }
    factory { provideRetrofitServices(get()) }
    single { provideRetrofit(get()) }
    single<SharedPreferences> { PreferenceManager.getDefaultSharedPreferences(androidContext()) }

    single<FirebaseStorage> {
        FirebaseStorage.getInstance()
    }
    single<FirebaseAuth>{
        FirebaseAuth.getInstance()
    }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl(BuildConfig.API_URL).client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(): OkHttpClient {
    return OkHttpClient().newBuilder().build()
}

fun provideRetrofitServices(retrofit: Retrofit): RetrofitServices = retrofit.create(RetrofitServices::class.java)
