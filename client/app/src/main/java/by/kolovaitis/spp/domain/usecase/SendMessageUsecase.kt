package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.repository.DataRepository

class SendMessageUsecase(private val dataRepository: DataRepository) {
    suspend operator fun invoke(id:Int, text:String){
        dataRepository.sendMessage(id, text)
    }
}