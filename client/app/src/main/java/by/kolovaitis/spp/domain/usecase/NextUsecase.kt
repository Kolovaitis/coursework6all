package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.repository.DataRepository

class NextUsecase(private val dataRepository: DataRepository) {
    suspend operator fun invoke(): Int? {
        return dataRepository.next()
    }
}