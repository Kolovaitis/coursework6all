package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.repository.DataRepository

class LikeUsecase(private val dataRepository: DataRepository) {
    suspend operator fun invoke(id:Int) {
       return dataRepository.likeUser(id)
    }
}