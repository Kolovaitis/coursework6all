package by.kolovaitis.spp.domain.repository

import by.kolovaitis.spp.domain.model.User

interface LoginRepository {
    suspend fun login(email:String, password:String)
    suspend fun logout()
    suspend fun register(email:String, password:String, user: User)
    fun isUserLogin():Boolean
}