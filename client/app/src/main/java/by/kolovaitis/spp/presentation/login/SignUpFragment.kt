package by.kolovaitis.spp.presentation.login

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kolovaitis.spp.databinding.SignUpFragmentBinding
import by.kolovaitis.spp.presentation.MainActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.datetime.datePicker
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.IOException
import java.util.*


class SignUpFragment : Fragment() {
    private lateinit var binding: SignUpFragmentBinding
    private val vm: SignUpViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SignUpFragmentBinding.inflate(inflater)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.email.setText(vm.email.value)
        binding.password.setText(vm.password.value)
        binding.goToSignIn.setOnClickListener {
            findNavController().popBackStack()
        }
        binding.email.addTextChangedListener {
            vm.emailHasChanged(it.toString())
        }
        binding.password.addTextChangedListener {
            vm.passwordHasChanged(it.toString())
        }
        binding.tvName.addTextChangedListener {
            vm.nameHasChanged(it.toString())
        }
        binding.tvPlace.addTextChangedListener {
            vm.placeHasChanged(it.toString())
        }
        binding.bSelectDate.setOnClickListener {
            MaterialDialog(requireContext()).show {
                datePicker { dialog, date ->
                    vm.dateHasChanged(date.time)
                }
            }
        }
        binding.tvDescription.addTextChangedListener {
            vm.descriptionHasChanged(it.toString())
        }
        binding.bSignUp.setOnClickListener {
            vm.login()
        }
        binding.imageView.setOnClickListener {
            selectImage()
        }
        with(vm) {
            date.observe(viewLifecycleOwner) {
                val current = Date()
                binding.tvAge.text = if (current.month >= it.month) {
                    current.year - it.year
                } else {
                    current.year - it.year - 1
                }.toString()

            }
            isButtonActive.observe(viewLifecycleOwner) {
                binding.bSignUp.isEnabled = it
            }
            wasError.observe(viewLifecycleOwner) {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
            successfullyLogin.observe(viewLifecycleOwner) {
                if (it) {
                    startActivity(
                        Intent(
                            context,
                            MainActivity::class.java
                        )
                    )
                    activity?.finish()
                }
            }
            isLoading.observe(viewLifecycleOwner) {
                binding.progressBar.visibility =
                    if (it) View.VISIBLE else View.GONE
            }
            image.observe(viewLifecycleOwner) {
                binding.imageView.setImageURI(it)
            }
        }
    }

    private fun selectImage() {

        // Defining Implicit Intent to mobile gallery
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(
            Intent.createChooser(
                intent,
                "Select Image from here..."
            ),
            PICK_IMAGE_REQUEST
        )
    }

    // Override onActivityResult method
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(
            requestCode,
            resultCode,
            data
        )
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {

            // Get the Uri of data
            val uri = data.data
            try {

                uri?.let { vm.selectBitmap(it) }
            } catch (e: IOException) {
                // Log the exception
                e.printStackTrace()
            }
        }
    }

    companion object {
        const val PICK_IMAGE_REQUEST = 22
    }
}