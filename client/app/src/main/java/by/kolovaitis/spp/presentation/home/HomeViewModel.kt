package by.kolovaitis.spp.presentation.home


import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import by.kolovaitis.spp.domain.model.User
import by.kolovaitis.spp.domain.usecase.*
import by.kolovaitis.spp.presentation.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class HomeViewModel(
    private val getNextUsecase: NextUsecase,
    private val getProfileUsecase: ProfileUsecase,
    private val likeUsecase: LikeUsecase,
    private val watchUsecase: WatchUsecase, logoutUsecase: LogoutUsecase, context: Context,
) : BaseViewModel(logoutUsecase, context) {
    private val _currentProfile: MutableLiveData<User> by lazy {
        MutableLiveData()
    }
    val currentProfile: LiveData<User> get() = _currentProfile

    private val _nextProfileId: MutableLiveData<Int> by lazy {
        MutableLiveData()
    }
    val nextProfileId: LiveData<Int> get() = _nextProfileId

    init {
        viewModelScope.launch(Dispatchers.IO) {
            runSafe {
                val firstId = getNextUsecase()
                _currentProfile.postValue(getProfileUsecase(firstId))
                _nextProfileId.postValue(getNextUsecase())
            }
        }
    }

    fun like() {
        viewModelScope.launch(Dispatchers.IO) {
            runSafe {
                currentProfile.value?.id.let {
                    likeUsecase(it!!)
                    next()
                }
            }
        }
    }

    fun dislike() {
        viewModelScope.launch(Dispatchers.IO) {
            runSafe {
                currentProfile.value?.id.let {
                    watchUsecase(it!!)
                    next()
                }
            }
        }
    }

    private suspend fun next() {
        _currentProfile.postValue(getProfileUsecase(nextProfileId.value))
        _nextProfileId.postValue(getNextUsecase())
    }


}