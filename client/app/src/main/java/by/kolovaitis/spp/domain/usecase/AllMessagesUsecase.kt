package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.model.Message
import by.kolovaitis.spp.domain.repository.DataRepository

class AllMessagesUsecase(private val dataRepository: DataRepository) {
    suspend operator fun invoke(id:Int): List<Message> {
        return dataRepository.allMessagesWith(id).sortedBy { it.date }
    }
}