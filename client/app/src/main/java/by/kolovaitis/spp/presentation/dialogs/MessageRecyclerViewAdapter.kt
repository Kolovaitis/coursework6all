package by.kolovaitis.spp.presentation.dialogs

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import by.kolovaitis.spp.R
import by.kolovaitis.spp.databinding.ItemMessageBinding
import by.kolovaitis.spp.domain.model.Message
import by.kolovaitis.spp.domain.model.User


/**
 * [RecyclerView.Adapter] that can display a [PlaceholderItem].
 * TODO: Replace the implementation with code for your data type.
 */
class MessageRecyclerViewAdapter(
    private var values: List<Message>,
    private val partner: UserData
) : RecyclerView.Adapter<MessageRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ItemMessageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        if(item.from == partner.id){
            holder.name.text = partner.name
            holder.card.setCardBackgroundColor(Color.parseColor("#BB86FC"))
        }else{
            holder.name.visibility = View.GONE
            holder.card.setCardBackgroundColor(Color.parseColor("#A5D6A7"))
        }
        holder.textView.text = item.text
    }

    override fun getItemCount(): Int = values.size
    fun update(new: List<Message>) {
        values = new
        notifyDataSetChanged()
    }

    inner class ViewHolder(binding: ItemMessageBinding) : RecyclerView.ViewHolder(binding.root) {
        val name: TextView = binding.tvAuthor
        val card: CardView = binding.cvMessage
        val textView: TextView = binding.tvText
    }

}