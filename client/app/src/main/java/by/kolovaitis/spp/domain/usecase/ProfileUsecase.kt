package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.model.User
import by.kolovaitis.spp.domain.repository.DataRepository

class ProfileUsecase(private val dataRepository: DataRepository) {
    suspend operator fun invoke(id:Int?=null): User {
        if(id==null){
            return dataRepository.profile()
        }
        return dataRepository.profile(id)
    }
}