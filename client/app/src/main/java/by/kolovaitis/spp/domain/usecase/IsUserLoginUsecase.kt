package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.repository.LoginRepository

class IsUserLoginUsecase(private val loginRepository: LoginRepository) {
    operator fun invoke()=
        loginRepository.isUserLogin()

}