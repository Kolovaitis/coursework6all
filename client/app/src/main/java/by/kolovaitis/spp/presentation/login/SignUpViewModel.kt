package by.kolovaitis.spp.presentation.login

import android.content.Context
import android.location.Geocoder
import android.net.Uri
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import by.kolovaitis.spp.domain.model.User
import by.kolovaitis.spp.domain.usecase.LogoutUsecase
import by.kolovaitis.spp.domain.usecase.RegisterUsecase
import by.kolovaitis.spp.presentation.base.BaseViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import java.util.*

class SignUpViewModel(
    private val registerUsecase: RegisterUsecase, context: Context,
    logoutUsecase: LogoutUsecase
) : BaseViewModel(logoutUsecase, context) {
    private val geocoder = Geocoder(context)
    private val _email: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val email: LiveData<String> get() = _email

    private val _password: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val password: LiveData<String> get() = _password

    private val _displayName: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val displayName: LiveData<String> get() = _displayName

    private val _description: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val description: LiveData<String> get() = _description

    private val _place: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val place: LiveData<String> get() = _place

    private val _date: MutableLiveData<Date> by lazy {
        MutableLiveData()
    }
    val date: LiveData<Date> get() = _date

    private val _isButtonActive: MediatorLiveData<Boolean> by lazy {
        MediatorLiveData<Boolean>().apply {
            val observer = Observer<Any> {
                if (_email.value.isNullOrEmpty() ||
                    _password.value.isNullOrEmpty() ||
                    _password.value!!.length < 5 ||
                    _place.value.isNullOrEmpty() ||
                    _description.value.isNullOrEmpty() ||
                    _displayName.value.isNullOrEmpty() ||
                    _date.value == null ||
                    isLoading.value == true
                ) {
                    this.postValue(false)
                } else {
                    this.postValue(true)
                }
            }
            this.addSource(_email, observer)
            this.addSource(_password, observer)
            this.addSource(_place, observer)
            this.addSource(_description, observer)
            this.addSource(_displayName, observer)
            this.addSource(_date, observer)
            this.addSource(isLoading, observer)

        }
    }
    val isButtonActive: LiveData<Boolean> get() = _isButtonActive


    private val _successfullyLogin: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val successfullyLogin: LiveData<Boolean> get() = _successfullyLogin

    private val _image: MutableLiveData<Uri> by lazy {
        MutableLiveData()
    }
    val image: LiveData<Uri> get() = _image

    fun login() {
        viewModelScope.launch(context = newSingleThreadContext("")) {
            runSafe {
                val location = geocoder.getFromLocationName(place.value, 1)[0]
                registerUsecase(
                    email.value!!,
                    password.value!!,
                    User(
                        name = displayName.value,
                        image = image.value,
                        description = description.value,
                        dateOfBirth = date.value,
                        longitude = location.longitude,
                        latitude = location.latitude
                    )
                )

                _successfullyLogin.postValue(true)

            }
        }
    }

    fun emailHasChanged(newEmail: String) {
        _email.postValue(newEmail)
    }

    fun passwordHasChanged(newPassword: String) {
        _password.postValue(newPassword)
    }

    fun nameHasChanged(newName: String) {
        _displayName.postValue(newName)
    }

    fun dateHasChanged(date: Date) {
        _date.postValue(date)
    }

    fun descriptionHasChanged(newThematics: String) {
        _description.postValue(newThematics)
    }

    fun placeHasChanged(newPlace: String) {
        _place.postValue(newPlace)
    }

    fun selectBitmap(uri: Uri) {
        _image.postValue(uri)
    }
}