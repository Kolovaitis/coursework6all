package by.kolovaitis.spp.data.datasource.impl

import android.net.Uri
import by.kolovaitis.spp.data.datasource.FirebaseDatasource
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import java.lang.Exception
import java.util.*

class FirebaseDatasourceImpl(storage: FirebaseStorage, private val auth: FirebaseAuth) :
    FirebaseDatasource {
    private val images = storage.reference
    override suspend fun register(email: String, password: String) {
        val task = auth.createUserWithEmailAndPassword(email, password)
        Tasks.await(task)
    }

    override suspend fun login(email: String, password: String) {
        val task = auth.signInWithEmailAndPassword(email, password)
        Tasks.await(task)
    }

    override suspend fun loadImage(uri: Uri): String {
        val dest = UUID.randomUUID().toString()
        images.child(dest).putFile(uri)
        return dest
    }

    override suspend fun getImageUrl(image: String): Uri? {
        val task = images.child(image).downloadUrl
        try {
            Tasks.await(task)
        } catch (e: Exception) {
            return null
        }
        return task.result
    }
}