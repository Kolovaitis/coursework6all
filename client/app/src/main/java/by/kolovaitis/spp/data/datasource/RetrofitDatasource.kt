package by.kolovaitis.spp.data.datasource

import by.kolovaitis.spp.data.model.*

interface RetrofitDatasource {
    suspend fun markUser(id: Int, mark: MarkData)
    suspend fun sendMessage(message: MessageData)
    suspend fun allMessagesWith(id: Int): List<MessageData>
    suspend fun next(): UserData
    suspend fun all(): List<UserData>
    suspend fun pairs(): List<Int>
    suspend fun profile(idUser: Int): UserData
    suspend fun profile(): UserData
    suspend fun logout()
    suspend fun login(credentials: LoginData)
    suspend fun register(credentials: RegisterData)
    fun isUserLogin():Boolean
}