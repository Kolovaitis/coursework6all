package by.kolovaitis.spp.data.model

data class ProfileResponse(
    val profile: UserData
)

class MessagesResponse(val messages: List<MessageData>)
data class ProfilesResponse(
    val profiles: List<UserData>
)

data class PairsResponse(
    val profiles: List<Int>
)

data class UserData(
    val name: String?,
    val image: String?,
    val description: String?,
    val id: Int,
    val date_of_birth: String?,
    val longitude: Double?,
    val latitude: Double?,
)

data class MessageData(
    val from_id: Int? = null,
    val to_id: Int? = null,
    val text: String? = null,
    val date: String? = null
)

data class MarkData(val like: Boolean)
