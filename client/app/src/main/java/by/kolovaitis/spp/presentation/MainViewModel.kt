package by.kolovaitis.spp.presentation


import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import by.kolovaitis.spp.domain.model.User
import by.kolovaitis.spp.domain.usecase.IsUserLoginUsecase
import by.kolovaitis.spp.domain.usecase.LogoutUsecase
import by.kolovaitis.spp.domain.usecase.ProfileUsecase
import by.kolovaitis.spp.presentation.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(
    private val isUserLoginUsecase: IsUserLoginUsecase,
    private val getProfileUsecase: ProfileUsecase, logoutUsecase: LogoutUsecase, context: Context,
) : BaseViewModel(logoutUsecase, context) {
    val isUserLogin: Boolean get() = isUserLoginUsecase()
    private val _currentUser: MutableLiveData<User> by lazy {
        MutableLiveData()
    }
    val currentUser: LiveData<User> get() = _currentUser

    init {
        viewModelScope.launch (Dispatchers.IO){
            runSafe { _currentUser.postValue(getProfileUsecase()) }
        }
    }
    fun logout(){
        viewModelScope.launch(Dispatchers.IO) {
            runSafe {
                logoutUsecase()
            }
        }
    }
}