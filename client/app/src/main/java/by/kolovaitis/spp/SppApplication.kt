package by.kolovaitis.spp

import android.app.Application
import by.kolovaitis.spp.modules.componentsModule
import by.kolovaitis.spp.modules.networkModule
import com.google.firebase.FirebaseApp
import com.yandex.mapkit.MapKitFactory
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class SppApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        MapKitFactory.setApiKey(BuildConfig.YANDEX_MAPKIT_KEY)
        MapKitFactory.initialize(this)

        FirebaseApp.initializeApp(this)
        startKoin {
            androidContext(applicationContext)
            modules(listOf(networkModule, componentsModule))
        }
    }
}