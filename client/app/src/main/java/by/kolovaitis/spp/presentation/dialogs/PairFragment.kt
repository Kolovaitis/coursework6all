package by.kolovaitis.spp.presentation.dialogs

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import by.kolovaitis.spp.R
import by.kolovaitis.spp.domain.model.User
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A fragment representing a list of Items.
 */
class PairFragment : Fragment() {
    private val vm: PairViewModel by viewModel()
    private var columnCount = 1
    private var adapter = PairRecyclerViewAdapter(emptyList()) {
        findNavController().navigate(
            R.id.messageFragment,
            bundleOf(
                "partner" to UserData(
                    id = it.id!!,
                    name = it.name,
                )
            )
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_pair_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = this@PairFragment.adapter
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.pairs.observe(viewLifecycleOwner) {
            adapter.update(it)
        }
    }

}