package by.kolovaitis.spp.data.datasource

import android.net.Uri

interface FirebaseDatasource {
    suspend fun register(email:String, password:String)
    suspend fun login(email: String, password: String)
    suspend fun loadImage(uri: Uri):String
    suspend fun getImageUrl(image:String):Uri?
}