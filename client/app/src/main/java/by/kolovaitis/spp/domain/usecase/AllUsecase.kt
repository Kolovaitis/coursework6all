package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.model.User
import by.kolovaitis.spp.domain.repository.DataRepository

class AllUsecase(private val dataRepository: DataRepository) {
    suspend operator fun invoke(): List<User> {
        return dataRepository.all()
    }
}