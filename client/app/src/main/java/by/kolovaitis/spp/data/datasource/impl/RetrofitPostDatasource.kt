package by.kolovaitis.spp.data.datasource.impl

import android.content.SharedPreferences
import android.util.Log
import by.kolovaitis.spp.data.RetrofitServices
import by.kolovaitis.spp.data.datasource.RetrofitDatasource
import by.kolovaitis.spp.data.model.*
import java.lang.Exception

class RetrofitPostDatasource(
    private val services: RetrofitServices,
    private val prefs: SharedPreferences,
) : RetrofitDatasource {
    private var cachedToken: String? = null
    private var token: String?
        get() = cachedToken ?: prefs.getString("token", null).apply { cachedToken = this }
        set(value: String?) {
            cachedToken = value
            if (value.isNullOrEmpty()) {
                prefs.edit().remove("token").apply()
            } else {
                prefs.edit().putString("token", value).apply()
            }
        }
    private val tokenNotNull: String get() = token ?: throw Exception("Not authorized")

    override suspend fun markUser(id: Int, mark: MarkData){
        services.mark(id = id, mark = mark, token = tokenNotNull)
    }

    override suspend fun sendMessage(message: MessageData) {
        services.sendMessage(id = message.to_id ?: 0, message = message, token = tokenNotNull)
    }

    override suspend fun allMessagesWith(id: Int): List<MessageData> {
        return services.getMessagesWith(id = id, token = tokenNotNull).messages
    }

    override suspend fun next(): UserData {
        return services.next(token = tokenNotNull)
    }

    override suspend fun all(): List<UserData> {
        return services.all(token = tokenNotNull).profiles
    }

    override suspend fun pairs(): List<Int> {
        return services.pairs(token = tokenNotNull).profiles
    }

    override suspend fun profile(idUser: Int): UserData {
        return services.profile(id = idUser, token = tokenNotNull).profile
    }

    override suspend fun profile(): UserData {
        val profile = services.profile(token = tokenNotNull)
        return profile.profile
    }

    override suspend fun logout() {
        token = ""
    }

    override suspend fun login(credentials: LoginData) {
        token = services.login(credentials).token
    }

    override suspend fun register(credentials: RegisterData) {
        token = services.registration(credentials).token
    }

    override fun isUserLogin()= token != null
}